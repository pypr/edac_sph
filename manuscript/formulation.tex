\section{The EDAC method}
\label{sec:edac-scheme}


The EDAC method is discussed in detail in~\cite{Clausen2013,Clausen2013a}. In
this method, the density of the fluid $\rho$ is held fixed and an evolution
equation for the pressure based on thermodynamic considerations is derived. As
a result, a pressure evolution equation needs to be solved in addition to the
momentum equation. The equations are,

\begin{align}
    \label{eq:mom}
    \frac{d \ten{u}}{d t} &= -\frac{1}{\rho} \nabla p +
    \text{div}(\ten{\sigma}), \\
    \frac{d p}{d t} &= -\rho c_s^2 \text{div}(\ten{u}) + \nu
    \nabla^2 p,
    \label{eq:p-evolve}
\end{align}
where $\ten{u}$ is the velocity of the fluid, $p$ is the pressure, $\sigma$ is
the deviatoric part of the stress tensor, $c_s$ is the speed of sound, and
$\nu$ is the kinematic viscosity of the fluid.

As is typically chosen in WCSPH schemes, the speed of sound is set to a
multiple of the maximum fluid velocity. In this paper $c_s = 10\ u_{\max}$
unless otherwise mentioned.

In this work, the fluid is assumed to be Newtonian, which results in the
following momentum equation:

\begin{align}
    \label{eq:mom-newt}
    \frac{d \ten{u}}{d t} &= -\frac{1}{\rho} \nabla p +
    \nu \nabla^2 \ten{u}.
\end{align}

On comparison of the EDAC method with the standard WCSPH formulation, it can
be seen that the momentum equation is unchanged and
equation~\eqref{eq:p-evolve} replaces the continuity equation,
$\frac{d\rho}{dt} = -\rho \ \text{div}(\ten{u})$. Also, owing to the pressure
evolution equation in EDAC, there is no need for an equation of state to
couple the fluid density and pressure.

The EDAC equation~\eqref{eq:p-evolve}, is derived in \cite{Clausen2013}. Two
simplifying assumptions are made in this derivation. The first is to ignore
the viscous dissipation term, $\phi$. The second is to set the Prandtl number
$Pr = \gamma$. The first assumption is also made in the case of the
traditional artificial compressibility schemes (as can be seen in equation 7
of \citet{Clausen2013}). Clausen~\cite{Clausen2013} shows that the artificial
compressibility equation results when isentropic flow is assumed and this
implies that the fluid is inviscid and therefore the viscous dissipation is
ignored. For the EDAC equation, instead of assuming isentropy, one drives the
density fluctuations to zero resulting in a different thermodynamic
relationship. The viscous dissipation is neglected to simplify the resulting
equations. What is crucial in the EDAC equation is the second derivative of
pressure which smoothes the pressure through the introduction of entropy. The
second assumption to set $Pr =\gamma$ is arbitrary and in the numerical
simulations for the current work, a numerical viscosity value is found to be
more well suited (see section~\ref{sec:choice-of-nu}).

In summary, the EDAC scheme essentially introduces entropy by damping the
pressure oscillations. It is to be noted that this is the only difference from
the WCSPH. As mentioned in the introduction, a similar approach is used in
\cite{antuono-deltasph:cpc:2010,marrone-deltasph:cmame:2011}, where a damping
term is introduced in the continuity equation. With the EDAC scheme, the
pressure is evolved directly rather than computed from the density. It is
important to note that the introduction of pressure damping is not directly
equivalent to adding an artificial viscosity to the momentum equation as the
pressure only affects the momentum equation via its gradient. The approach is
similar to the density filtering approach (see
\cite{wcsph-state-of-the-art-2010} for more details). However, the pressure
field is smoothed at every step in the present case and not after every $m$
steps.

In the next section, an SPH-discretization of these equations is performed to
obtain the numerical scheme.

\section{Numerical implementation}
\label{sec:implementation}

As discussed in the introduction, there are two major issues that arise when
using weakly-compressible SPH (WCSPH) formulations. The first is the presence
of large pressure oscillations due to the stiff equation of state and the
second is due to the inhomogeneous particle distributions. The basic EDAC
formulation solves the first problem~\cite{PRKP:edac-sph-iccm2015}. The TVF
scheme solves the second problem by the introduction of a background pressure
for internal flows. Based on this, two different formulations using the EDAC
are presented in the following. The first formulation is what is called the
\emph{standard EDAC formulation}. This formulation can be used for external
flows. The second formulation is what is called the \emph{EDAC TVF
  formulation}, which is based on the TVF formulation and can be applied to
internal flows where it is possible to use a background pressure. Numerical
discretizations for both these schemes are discussed next.

\subsection{The standard EDAC formulation}
\label{sec:edac-ext}

The EDAC formulation keeps the density constant and this eliminates the need
for the continuity equation or the use of a summation density to find the
pressure. However, in SPH discretizations, $m/\rho$ is typically used as a
proxy for the particle volume. The density of the fluids can therefore be
computed using the summation density approach. This density does not directly
affect the pressure as there is no equation of state. In the case of solid
walls, the density of any wall particle is set to a constant. The classic
summation density equation for SPH is recalled:

\begin{equation}
  \label{eq:summation-density}
  \rho_i = \sum_j m_j W_{ij},
\end{equation}
where $W_{ij} = W(|\ten{r_i} - \ten{r_j}|, h)$ is the kernel function chosen
for the SPH discretization and $h$ is the kernel radius parameter. In this
paper, the quintic spline kernel is used, which is given by,

\begin{equation}
\label{eq:quintic-spline}
W(q) = \left \{
\begin{array}{ll}
  \alpha_2 \left[ {(3-q)}^5 - 6{(2-q)}^5 + 15{(1-q)}^5 \right],\
  & \textrm{for} \ 0\leq q \leq 1,\\
  \alpha_2 \left[ {(3-q)}^5 - 6{(2-q)}^5 \right],
  & \textrm{for} \ 1 < q \leq 2,\\
  \alpha_2 \ {(3-q)}^5 , & \textrm{for} \ 2 < q \leq 3,\\
  0, & \textrm{for} \ q>3,\\
\end{array} \right.
\end{equation}
where $\alpha_2 = 7/(478\pi h^2)$ in two-dimensions, and $q=|\ten{r}|/h$.

In the previous work~\cite{PRKP:edac-sph-iccm2015}, Monaghan's original
formulation was used for the pressure gradient and the formulation due to
\citet{morris-lowRe-97} was used for the viscous term in
equation~\eqref{eq:mom-newt}. The method of \citet{Adami2012} was used to
implement the effect of boundaries.

In the present work, a number density based formulation is employed as used in
\cite{Adami2012}, which results in the following momentum equation:

\begin{align}
  \label{eq:mom-sph}
  \frac{d \ten{u}_i}{d t} = \frac{1}{m_i} \sum_j \left(V_i^2 + V_j^2 \right)
  \left[ - \tilde{p}_{ij} \nabla W_{ij} + \tilde{\eta}_{ij}
    \frac{\ten{u}_{ij}}{(r_{ij}^2 + \eta h_{ij}^2)} \nabla W_{ij}\cdot
    \ten{r}_{ij} \right] + \ten{g}_i,
\end{align}
where $\ten{r}_{ij} = \ten{r}_i - \ten{r}_j$, $\ten{u}_{ij} = \ten{u}_i -
\ten{u}_j$, $h_{ij} = (h_i + h_j)/2$, $\eta=0.01$,

\begin{equation}
  \label{eq:number-density}
  V_i = \frac{1}{\sum_j W_{ij}},
\end{equation}
\begin{equation}
  \label{eq:tvf-p-ij}
  \tilde{p}_{ij} = \frac{\rho_j p_i + \rho_i p_j}{\rho_i + \rho_j},
\end{equation}
\begin{equation}
  \label{eq:tvf-eta-ij}
  \tilde{\eta}_{ij} = \frac{2 \eta_i \eta_j}{\eta_i + \eta_j},
\end{equation}
where $\eta_i = \rho_i \nu_i$.

The EDAC pressure evolution equation~(\ref{eq:p-evolve}) is discretized using
a similar approach to the momentum equation to be,

\begin{align}
    \label{eq:p-edac}
    \frac{d p_i }{dt} &=  \sum_j \frac{m_j \rho_i}{\rho_j} c_s^2 \ \ten{u_{ij}}
    \cdot \nabla W_{ij} + \frac{(V_i^2 + V_j^2)}{m_i} \tilde{\eta}_{ij}
    \frac{p_{ij}}{(r_{ij}^2 + \eta h_{ij}^2)} \nabla W_{ij}\cdot \ten{r}_{ij},
\end{align}
where $p_{ij} = p_i - p_j$.  The particles are moved according to,

\begin{align}
  \label{eq:motion}
  \frac{d \ten{r}_i}{d t} = \ten{u}_i.
\end{align}

Upon the specification of suitable initial conditions for $\ten{u}$, $p$, $m$,
and $\ten{r}$,
equations~\eqref{eq:summation-density},~\eqref{eq:mom-sph},~\eqref{eq:p-edac},
and~\eqref{eq:motion} are sufficient for simulating the flow in the absence of
any boundaries.

\subsection{EDAC TVF formulation}
\label{sec:edac-int}

In WCSPH, as the particles move they tend to become disordered. This
introduces significant errors in the simulation. The particle positions can be
regularized by the addition of a background pressure. A naive approach would
be to simply add a constant pressure and use it in the governing equations.
However, as shown by \citet{sph:basa-etal-2009}, the error in computing the
gradient of pressure increases when the pressure values are large. They
subtract the average pressure to reduce this error. The TVF scheme of
\citet{Adami2013} overcomes this by advecting the particles using an arbitrary
background pressure through the ``transport velocity'' and correct for this
background pressure using an additional stress term in the momentum equation.
This ensures a homogeneous particle distribution without introducing a
constant background pressure in the pressure derivative term.

For internal flows, the TVF formulation is adapted to introduce the background
pressure. The density is computed using the summation density
equation~(\ref{eq:summation-density}). As before, this is mainly to serve as a
proxy for the particle volume in the SPH discretizations. The momentum
equation for the TVF scheme as discussed in \citet{Adami2013} is given by,

\begin{equation}
  \label{eq:tvf-momentum}
  \begin{split}
    \frac{\tilde{d} \ten{u}_i}{d t} = \frac{1}{m_i} \sum_j \left( V_i^2 +
    V_j^2 \right) & \left[ - \tilde{p}_{ij} \nabla W_{ij} +
      \frac{1}{2}(\ten{A}_i + \ten{A}_j) \cdot \nabla W_{ij} \right . \\ &
      \left .  + \tilde{\eta}_{ij} \frac{\ten{u}_{ij}}{(r_{ij}^2 + \eta
        h_{ij}^2)} \nabla W_{ij}\cdot \ten{r}_{ij} \right] + \ten{g}_i,
  \end{split}
\end{equation}
where $\ten{A} = \rho \ten{u}(\ten{\tilde{u}} - \ten{u})$, $\ten{\tilde{u}}$
is the advection or transport velocity and the material derivative,
$\frac{\tilde{d}}{dt}$ is given as,


\begin{align}
    \label{eq:tvf-derivative}
    \frac{\tilde{d}( \cdot) }{d t} = \frac{\partial (\cdot) }{\partial t} +
  \ten{\tilde{u}} \cdot \text{grad} (\ten{\cdot}).
\end{align}
Thus the particles move using the transport velocity,

\begin{equation}
  \label{eq:advection}
  \frac{d \ten{r}_i}{dt} = \ten{\tilde{u}}_i.
\end{equation}
The transport velocity is obtained from the momentum velocity $\ten{u}$ at
each time step using,

\begin{equation}
  \label{eq:transport-vel}
  \ten{\tilde{u}}_i(t + \delta t) = \ten{u}_i(t) +
  \delta t \left(
    \frac{\tilde{d} \ten{u}_i}{dt}
    - \frac{p_b}{m_i} \sum_j \left( V_i^2 + V_j^2 \right)
    \nabla W_{ij}
    \right),
\end{equation}
where $p_b$ is the background pressure.

In the TVF scheme, the pressure is computed from the density using the
standard equation of state with a value of $\gamma=1$. Instead, the EDAC
equation~(\ref{eq:p-edac}) is used to evolve the pressure. In the present
approach, the pressure reduction technique proposed by
\citet{sph:basa-etal-2009} is used to mitigate the errors due to large
pressures. This requires the computation of the average pressure of each
particle, $p_{\text{avg}}$:

\begin{equation}
  \label{eq:pavg}
  p_{\text{avg}, i} = \sum_{j=1}^{N_i} \frac{p_j}{N_i},
\end{equation}
where $N_i$ are the number of neighbors for the particle $i$ and includes both
fluid and boundary neighbors. Equation~(\ref{eq:tvf-p-ij}) is then replaced
with,

\begin{equation}
  \label{eq:tvf-p-ij-basa}
  \tilde{p}_{ij} =
  \frac{\rho_j (p_i-p_{avg, i}) + \rho_i (p_j - p_{avg, i})}{\rho_i + \rho_j}.
\end{equation}

In Section~\ref{sec:results} it can be seen that this results in significantly
improved results that outperform the traditional TVF scheme. It is worth
mentioning that this technique, applied to the standard SPH or to the standard
TVF scheme does not result in any significant improvement.

The boundary conditions are satisfied using the formulation of
\citet{Adami2012}. This method uses fixed wall particles and sets the pressure
and velocity of these wall particles in order to accurately simulate the
boundary conditions. The same scheme is used here with the only modification
being that the density of the boundary particles is not set based on the
pressure of the boundary particles (i.e.\ equation~(28) in \citet{Adami2012}
is not used).

\subsection{Suitable choice of $\nu$ for EDAC}
\label{sec:choice-of-nu}

In equation~\eqref{eq:p-edac} one can see that the viscosity $\nu$ is used to
diffuse the pressure. The original formulation assumes that the value of $\nu$
is the same as the fluid viscosity. In our numerical experiments it was found
that if the viscosity is too small, the pressure builds up too fast and
eventually blows up. If the viscosity is too large it diffuses too fast
resulting in a non-physical simulation. Thus, the physical viscosity is not
always the most appropriate. Instead using,

\begin{equation}
  \label{eq:edac-nu}
  \nu_{edac} = \frac{\alpha h c_s}{8},
\end{equation}
works very well. The choice of $\nu_{edac}$ is motivated by the expression for
artificial viscosity in traditional WCSPH formulations. The form of the
viscous term used in
\cite{antuono-deltasph:cpc:2010,marrone-deltasph:cmame:2011} is also the same.
In this paper, it is found that $\alpha=0.5$ is a good choice for a wide range
of Reynolds numbers (0.0125 to 10000). While this choice of $\nu_{edac}$ is
motivated by the expression for artificial viscosity traditionally used in the
SPH, the viscous damping of pressure is not the same as adding artificial
viscosity directly to the momentum equation.

To summarize the schemes,
\begin{itemize}
\item for external flow problems,
  equations~\eqref{eq:summation-density},~\eqref{eq:mom-sph},
  and~\eqref{eq:p-edac} are used. The particles move with the fluid velocity
  $\ten{u}$ and are advected according to~\eqref{eq:motion}.

\item for internal flows,
  equations~\eqref{eq:summation-density},~\eqref{eq:tvf-momentum},~\eqref{eq:pavg},
  \eqref{eq:tvf-p-ij-basa} and~\eqref{eq:p-edac} are used.
  Equation~\eqref{eq:advection} is used to advect the particles. The transport
  velocity is found from equation~\eqref{eq:transport-vel}.
\end{itemize}
For each of the schemes, the value of $\nu$ used in the
equation~\eqref{eq:p-edac} is found using equation~\eqref{eq:edac-nu}. The
value of $\nu$ used in the momentum equation is the fluid viscosity.

The proposed EDAC scheme is explicit and as such, any suitable integrator can
be used. In this work, one of the two simplest possible two-stage explicit
integrators is chosen. For both integrators, the particle properties are first
predicted at $t + \delta t/2$. The right-hand-side (RHS) is subsequently
evaluated at this intermediate step and the final properties at $t + \delta t$
are obtained by correcting the predicted values. Two variants of this
predictor-corrector integration scheme are defined. In the first type, the
prediction stage is completed using the RHS from the previous time-step. This
is called the Predict-Evaluate-Correct (PEC) type integrator. In the second
variant, an evaluation of the RHS is carried out for the predictor stage. This
integrator, deemed Evaluate-Predict-Evaluate-Correct (EPEC) is more accurate
(at the cost of two RHS evaluations per time-step).  For the standard EDAC
scheme the time integration proceeds as follows.  The predictor step is first
performed as,

\begin{equation}
  \label{eq:integrate-edac-pred}
  \begin{aligned}
  \ten{u}^{n+\frac{1}{2}} &= \ten{u}^n + \frac{\Delta t}{2m} \ten{f}^{n-\frac{1}{2}} \\
  \ten{r}^{n+\frac{1}{2}} &= \ten{r}^n + \frac{\Delta t}{2} \ten{u}^{n-\frac{1}{2}} \\
  p^{n+\frac{1}{2}} & = p^n + \frac{\Delta t}{2}\ a_p^{n-\frac{1}{2}},
\end{aligned}
\end{equation}

\noindent where $a_p$ is the right hand side of equation~\eqref{eq:p-edac}. The new
accelerations are then computed at this point and the corrector step is as,

\begin{equation}
  \label{eq:integrate-edac-corr}
  \begin{aligned}
  \ten{u}^{n+1} &= \ten{u}^n + \frac{\Delta t}{m} \ \ten{f}^{n+\frac{1}{2}} \\
  \ten{r}^{n+1} &= \ten{r}^n + \frac{\Delta t}{m} \ \ten{u}^{n+\frac{1}{2}} \\
  p^{n+1} & = p^n + \Delta t \ a_p^{n+\frac{1}{2}}.
\end{aligned}
\end{equation}

\noindent For the EDAC TVF method, the predictor step is implemented as,

\begin{equation}
  \label{eq:integrate-edac-tvf-pred}
  \begin{aligned}
    \ten{u}^{n+\frac{1}{2}} &= \ten{u}^n + \frac{\Delta t}{2m} \  \ten{f}^{n-\frac{1}{2}} \\
    \ten{\tilde{u}}^{n+\frac{1}{2}} &= \ten{u}^{n+\frac{1}{2}} +
    \frac{\Delta t}{2m}  \ \ten{f_{p_b}}^{n-\frac{1}{2}} \\
  \ten{r}^{n+\frac{1}{2}} &= \ten{r}^n + \frac{\Delta t}{2} \  \ten{\tilde{u}}^{n+\frac{1}{2}} \\
  p^{n+\frac{1}{2}} & = p^n + \frac{\Delta t}{2}\ a_p^{n-\frac{1}{2}},
\end{aligned}
\end{equation}

\noindent where $\ten{f}_{p_b}$ is the background pressure force.  At this point the
accelerations are computed and the corrector step is performed as,

\begin{equation}
  \label{eq:integrate-edac-tvf-corr}
  \begin{aligned}
  \ten{u}^{n+1} &= \ten{u}^n + \frac{\Delta t}{m} \  \ten{f}^{n+\frac{1}{2}} \\
  \ten{\tilde{u}}^{n+1} &= \ten{u}^{n+1} + \frac{\Delta t}{m} \  \ten{f_{p_b}}^{n+\frac{1}{2}} \\
  \ten{r}^{n+1} &= \ten{r}^n + \Delta t \ \ten{\tilde{u}}^{n+1} \\
  p^{n+1} & = p^n + \Delta t \ a_p^{n+\frac{1}{2}}.
\end{aligned}
\end{equation}


As mentioned in the introduction, all the equations and algorithms presented
in this work are implemented using the PySPH
framework~\cite{PRKP:PySPH-particles13,PR:pysph:scipy16,pysph}. PySPH is an
open source framework for SPH that is written in Python. It is easy to use,
easy to extend, and supports non-intrusive parallelization and dynamic load
balancing. PySPH provides an implementation of the TVF formulation and this
allows for a comparison of the results with those of the standard SPH and TVF
where necessary. In the next section, the performance of the proposed SPH
scheme is evaluated for several benchmark problems of varying complexity.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "edac_sph"
%%% fill-column: 78
%%% End:
