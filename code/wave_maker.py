from __future__ import print_function

import numpy

from pysph.base.kernels import QuinticSpline
from pysph.sph.equation import Equation
from pysph.sph.scheme import AdamiHuAdamsScheme, WCSPHScheme, SchemeChooser
from pysph.sph.integrator_step import TwoStageRigidBodyStep, WCSPHStep
from pysph.base.utils import get_particle_array_wcsph
from pysph.solver.application import Application

from edac import EDACScheme


# Tank geometry
fluid_column_height = 0.27
fluid_column_width = 0.5
container_height = 0.6
container_width = 0.5

# numerical parameters
nboundary_layers = 4
hdx = 1.0
dx = 0.004
h = hdx*dx
ro = 1000.0
co = 10.0 * numpy.sqrt(2*9.81*fluid_column_height)
gamma = 7.0
alpha = 0.25
beta = 0.0
B = co*co*ro/gamma
p0 = 1000.0
# d is free surface hight, omega is frequency, k is wave number, S is stroke length
d = 0.27
omega = 2*numpy.pi/1.3
k = 2*numpy.pi/1.89
H = 0.1
S = H*(numpy.sinh(k*d)*numpy.cosh(k*d)+k*d)/(2*numpy.sinh(k*d)*numpy.sinh(k*d))

print("dt should be less than", h/(4.0*1.1*co))


class PistonAccleration(Equation):
    def __init__(self, dest, sources):
        super(PistonAccleration, self).__init__(dest, sources)
        self.omega=omega
        self.S=S

    def initialize(self, d_idx, d_au, t):
        d_au[d_idx] = -0.5*self.omega*self.omega*self.S*sin(self.omega*t)


class WaveMaker(Application):

    def add_user_options(self, group):
        group.add_argument(
            "--edac-xsph-eps", action="store", type=float,
            dest="xsph_eps", default=0.5,
            help="eps factor to use for XSPH correction for EDAC case."
        )

    def _create_boundary(self,x1,y1,x2,y2,dx):
        yr = numpy.arange(y1,y2+dx/2, dx)
        xr = numpy.ones_like(yr) * x2
        xb = numpy.arange(x1+dx, x2-dx+dx/2, dx)
        yb = numpy.ones_like(xb) * y1
        x = numpy.concatenate( [xb, xr] )
        y = numpy.concatenate( [yb, yr] )
        return x, y

    def create_particles(self):
        _xre = []; _yre = []
        for i in range(nboundary_layers):
            xb, yb = self._create_boundary(
                x1=-0.2, y1=-i*dx, x2=container_width+i*dx,
                y2=container_height, dx=dx
            )

            _xre.append(xb); _yre.append(yb)

        xb = numpy.concatenate(_xre); yb = numpy.concatenate(_yre)
        boundary=get_particle_array_wcsph(name='boundary',x=xb,y=yb)
        boundary.h[:]=numpy.ones_like(xb)*hdx*dx
        boundary.m[:] = dx * dx * ro
        boundary.rho[:] = ro
        boundary.gid[:] = list(range( boundary.get_number_of_particles()))
        # creating movable boundary
        xp,yp=numpy.mgrid[-3*dx:dx:dx,dx:container_height:dx]
        xp=xp.ravel()
        yp=yp.ravel()
        piston=get_particle_array_wcsph(name='piston',x=xp,y=yp)
        piston.h[:]=numpy.ones_like(xp)*hdx*dx
        piston.m[:] = dx * dx * ro
        piston.rho[:] = ro

        piston.u[:] = 0.5*omega*S
        piston.gid[:] = list(range( piston.get_number_of_particles()))

        # creating fluid
        xf,yf=numpy.mgrid[dx:fluid_column_width:dx,dx:fluid_column_height:dx]
        xf=xf.ravel()
        yf=yf.ravel()
        fluid=get_particle_array_wcsph(name='fluid',x=xf,y=yf)
        fluid.h[:] = numpy.ones_like(xf) * hdx * dx
        fluid.m[:] = dx * dx * ro
        fluid.rho[:] = ro
        fluid.gid[:] = list(range( fluid.get_number_of_particles()))
        print("2D Wave Generator with %d fluid, %d boundary particles"%(
            fluid.get_number_of_particles(),
            boundary.get_number_of_particles()
        ))

        self.scheme.setup_properties((fluid, piston, boundary))
        for prop in 'x0 y0 z0 u0 v0 w0'.split():
            piston.add_property(prop)

        for pa in (fluid, piston, boundary):
            if 'V' in pa.properties:
                pa.V[:] = 1.0/(dx*dx)

        return [boundary, fluid, piston]

    def create_scheme(self):
        edac = EDACScheme(
            fluids=['fluid'], solids=['boundary', 'piston'], dim=2, c0=co,
            rho0=ro, pb=0.0, nu=0.0, gy=-9.81, eps=0.5, h=h,
            artificial_alpha=alpha
        )
        wcsph = WCSPHScheme(
            fluids=['fluid'], solids=['boundary', 'piston'], dim=2, c0=co,
            rho0=ro, h0=h, hdx=hdx, nu=0.0, gy=-9.81, gamma=gamma,
            alpha=alpha, beta=beta, hg_correction=True
        )
        aha = AdamiHuAdamsScheme(
            fluids=['fluid'], solids=['boundary', 'piston'], dim=2, rho0=ro,
            c0=co, h0=h, nu=0.0, gy=-9.81, alpha=alpha, gamma=7.0
        )
        return SchemeChooser(
            default='edac', wcsph=wcsph, edac=edac, aha=aha
        )

    def configure_scheme(self):
        dt = 5e-5
        tf = 5.0
        kernel = QuinticSpline(dim=2)
        if self.options.scheme == 'edac':
            extra_steppers = dict(piston=TwoStageRigidBodyStep())
            integrator_cls = None
            self.scheme.configure(
                artificial_alpha=self.options.alpha,
                eps=self.options.xsph_eps
            )
        elif self.options.scheme == 'aha':
            extra_steppers = dict(piston=TwoStageRigidBodyStep())
            integrator_cls = None
        else:
            from pysph.sph.integrator import EPECIntegrator
            extra_steppers = None
            #integrator_cls = None
            integrator_cls = EPECIntegrator

        self.scheme.configure_solver(
            kernel=kernel, integrator_cls=integrator_cls,
            extra_steppers=extra_steppers, dt=dt, tf=tf, pfreq=1000
        )

    def create_equations(self):
        eq = self.scheme.get_equations()
        g0 = eq[0]
        g0.equations.append(
            PistonAccleration(dest='piston', sources=['piston'])
        )
        if self.options.scheme in ['wcsph']:
            from pysph.sph.basic_equations import XSPHCorrection
            gl = eq[-1]
            gl.equations.append(
                XSPHCorrection(dest='piston', sources=['piston'], eps=0.0)
            )
        return eq

    def post_process(self, info_fname):
        try:
            import matplotlib
            matplotlib.use('Agg')
            from matplotlib import pyplot as plt
        except ImportError:
            print("Post processing requires matplotlib.")
            return
        if self.rank > 0:
            return
        info = self.read_info(info_fname)
        if len(self.output_files) == 0:
            return

        import os
        from pysph.solver.utils import load
        from pysph.tools.interpolator import Interpolator
        fname = self.output_files[-1]
        data = load(fname)
        y = numpy.linspace(0, 0.3)
        x = 0.25*numpy.ones_like(y)
        interp = Interpolator(data['arrays'].values(), x=x, y=y)
        p = interp.interpolate('p')
        plt.plot(y, p)
        plt.xlabel('y'); plt.ylabel('p')
        fig = os.path.join(self.output_dir, 'p_vs_y.png')
        plt.savefig(fig, dpi=300)
        plt.close()
        res = os.path.join(self.output_dir, 'results.npz')
        t = data['solver_data']['t']
        p_real = data['arrays']['fluid'].p
        numpy.savez(
            res, t=t, x=x, y=y, p=p, p_min=min(p_real), p_max=max(p_real)
        )


if __name__ == '__main__':
    app = WaveMaker()
    app.run()
    app.post_process(app.info_filename)
