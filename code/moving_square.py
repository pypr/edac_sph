from pysph.sph.integrator_step import TwoStageRigidBodyStep

# the equations
from pysph.sph.equation import Group
from edac import EDACScheme, EDACStep

from pysph.examples.spheric import moving_square


class MovingSquare(moving_square.MovingSquare):
    def create_particles(self):
        fluid, solid, obstacle = super(MovingSquare, self).create_particles()
        self.scheme.setup_properties([fluid, solid, obstacle])
        solid.add_output_arrays(['ug', 'vg', 'uf', 'vf'])
        obstacle.add_output_arrays(['ug', 'vg', 'uf', 'vf'])
        return [fluid, solid, obstacle]

    def consume_user_options(self):
        super(MovingSquare, self).consume_user_options()
        c0 = moving_square.c0
        nu = moving_square.nu
        self.scheme = EDACScheme(
            fluids=['fluid'], solids=['solid', 'obstacle'], c0=c0, nu=nu,
            rho0=moving_square.rho0, pb=moving_square.p0
        )

        dt = moving_square.dt
        tf = moving_square.tf

        extra_steppers = dict(obstacle=TwoStageRigidBodyStep())
        self.scheme.configure_solver(
            dim=2, extra_steppers=extra_steppers,
            tf=tf, dt=dt, adaptive_timestep=False,
            output_at_times=[1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0])
        )

    def create_equations(self):
        # set the acceleration for the obstacle using the special function
        # mimicking the accelerations provided in the test.
        move_obstacle = Group(
            equations=[
                moving_square.SPHERICBenchmarkAcceleration(
                    dest='obstacle', sources=None
                ),
            ],
            real=False
        )
        equations = self.scheme.get_equations()
        equations.insert(0, move_obstacle)

        return equations

if __name__ == '__main__':
    app = MovingSquare()
    app.run()
